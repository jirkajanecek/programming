﻿using System;

namespace DesignPatterns.AbstractFactory.General.Products
{
	public class ConcreteProductAType2 : IProductA
	{
		public void DoStuffA()
		{
			Console.WriteLine($"{nameof(ConcreteProductAType2)}::{nameof(DoStuffA)}");
		}
	}
}
