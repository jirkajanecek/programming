﻿namespace DesignPatterns.AbstractFactory.General.Products
{
	public interface IProductA
	{
		void DoStuffA();
	}
}
