﻿using System;

namespace DesignPatterns.AbstractFactory.General.Products
{
	public class ConcreteProductBType2 : IProductB
	{
		public void DoStuffB()
		{
			Console.WriteLine($"{nameof(ConcreteProductBType2)}::{nameof(DoStuffB)}");
		}
	}
}
