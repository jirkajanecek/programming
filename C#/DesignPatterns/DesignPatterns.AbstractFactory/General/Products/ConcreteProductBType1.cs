﻿using System;

namespace DesignPatterns.AbstractFactory.General.Products
{
	public class ConcreteProductBType1 : IProductB
	{
		public void DoStuffB()
		{
			Console.WriteLine($"{nameof(ConcreteProductBType1)}::{nameof(DoStuffB)}");
		}
	}
}
