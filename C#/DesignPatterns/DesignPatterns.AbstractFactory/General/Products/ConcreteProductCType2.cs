﻿using System;

namespace DesignPatterns.AbstractFactory.General.Products
{
	public class ConcreteProductCType2 : IProductC
	{
		public void DoStuffC()
		{
			Console.WriteLine($"{nameof(ConcreteProductCType2)}::{nameof(DoStuffC)}");
		}
	}
}
