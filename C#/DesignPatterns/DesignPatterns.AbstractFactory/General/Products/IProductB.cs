﻿namespace DesignPatterns.AbstractFactory.General.Products
{
	public interface IProductB
	{
		void DoStuffB();
	}
}
