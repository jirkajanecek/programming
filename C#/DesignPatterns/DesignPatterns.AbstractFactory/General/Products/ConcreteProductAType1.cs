﻿using System;

namespace DesignPatterns.AbstractFactory.General.Products
{
	public class ConcreteProductAType1 : IProductA
	{
		public void DoStuffA()
		{
			Console.WriteLine($"{nameof(ConcreteProductAType1)}::{nameof(DoStuffA)}");
		}
	}
}
