﻿using System;

namespace DesignPatterns.AbstractFactory.General.Products
{
	public class ConcreteProductCType1 : IProductC
	{
		public void DoStuffC()
		{
			Console.WriteLine($"{nameof(ConcreteProductCType1)}::{nameof(DoStuffC)}");
		}
	}
}
