﻿using DesignPatterns.AbstractFactory.General.Creators;

namespace DesignPatterns.AbstractFactory.General
{
	public interface ICreatorDispatcher
	{
		ICreator Dispatch(ProductType productType);
	}
}
