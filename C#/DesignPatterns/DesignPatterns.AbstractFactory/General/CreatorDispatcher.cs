﻿using System;
using DesignPatterns.AbstractFactory.General.Creators;

namespace DesignPatterns.AbstractFactory.General
{
	public class CreatorDispatcher : ICreatorDispatcher
	{
		public ICreator Dispatch(ProductType productType)
		{
			ICreator creator = null;

			switch(productType)
			{
				case ProductType.Type1:
					creator = new ConcreteCreatorType1();
					break;

				case ProductType.Type2:
					creator = new ConcreteCreatorType2();
					break;

				default:
					throw new ArgumentException($"Product type {productType.ToString()} is not supported");
			}

			return creator;
		}
	}
}
