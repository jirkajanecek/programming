﻿using DesignPatterns.AbstractFactory.General.Products;

namespace DesignPatterns.AbstractFactory.General.Creators
{
	public class ConcreteCreatorType1 : ICreator
	{
		public IProductA CreateProductA()
		{
			var product = new ConcreteProductAType1();
			return product;
		}

		public IProductB CreateProductB()
		{
			var product = new ConcreteProductBType1();
			return product;
		}

		public IProductC CreateProductC()
		{
			var product = new ConcreteProductCType1();
			return product;
		}
	}
}
