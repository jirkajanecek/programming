﻿using DesignPatterns.AbstractFactory.General.Products;

namespace DesignPatterns.AbstractFactory.General.Creators
{
	public class ConcreteCreatorType2 : ICreator
	{
		public IProductA CreateProductA()
		{
			var product = new ConcreteProductAType2();
			return product;
		}

		public IProductB CreateProductB()
		{
			var product = new ConcreteProductBType2();
			return product;
		}

		public IProductC CreateProductC()
		{
			var product = new ConcreteProductCType2();
			return product;
		}
	}
}
