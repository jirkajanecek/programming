﻿using DesignPatterns.AbstractFactory.General.Products;

namespace DesignPatterns.AbstractFactory.General.Creators
{
	public interface ICreator
	{
		IProductA CreateProductA();

		IProductB CreateProductB();

		IProductC CreateProductC();
	}
}
