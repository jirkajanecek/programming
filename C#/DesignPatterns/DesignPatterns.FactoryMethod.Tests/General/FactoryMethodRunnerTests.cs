﻿using DesignPatterns.FactoryMethod.General;
using System;
using Xunit;
using FluentAssertions;

namespace DesignPatterns.FactoryMethod.Tests.General
{
	public class FactoryMethodRunnerTests
    {
		private ICreatorDispatcher dispatcher = null;

		public FactoryMethodRunnerTests()
		{
			dispatcher = new CreatorDispatcher();
		}

		[Theory]
		[InlineData(ProductType.ProductA, typeof(ConcreteProductA))]
		[InlineData(ProductType.ProductB, typeof(ConcreteProductB))]
		public void Create_FactoryMethod_Success(ProductType productType, Type expectedProductType)
		{
			var creator = dispatcher.Dispatch(productType);
			var product = creator.CreateProduct();

			var typeName = product.GetType().ToString();
			typeName.Should().BeEquivalentTo(expectedProductType.ToString(), "Type name should be the same");
		}

		[Fact]
		public void Create_FactoryMethod_UnknownType_Throws()
		{
			Action action = () => dispatcher.Dispatch((ProductType)(-1));
			action.Should().Throw<ArgumentException>();
		}
    }
}
