﻿using System;

namespace DesignPatterns.FactoryMethod.General
{
	/// <summary>
	/// Common abstract class for specific product creation classes, which implements ICreator.
	/// </summary>
	public abstract class Creator : ICreator
	{
		/// <summary>
		/// Abstract method for product creation
		/// </summary>
		/// <returns>Concrete product, which implements IProduct interface</returns>
		public abstract IProduct CreateProduct();

		/// <summary>
		/// Method, which contains common functionality for Creator like classes.
		/// </summary>
		public void DoSomeCommonCreatorStuff()
		{
			Console.WriteLine($"{nameof(Creator)}::{nameof(DoSomeCommonCreatorStuff)}: processing some common stuff.");
		}
	}
}
