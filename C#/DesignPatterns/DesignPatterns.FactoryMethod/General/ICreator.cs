﻿namespace DesignPatterns.FactoryMethod.General
{
	/// <summary>
	/// Common interface for product creation
	/// </summary>
	public interface ICreator
    {
		/// <summary>
		/// Creates product
		/// </summary>
		/// <returns>Created product</returns>
		IProduct CreateProduct();

		/// <summary>
		/// Some other common functionality 
		/// </summary>
		void DoSomeCommonCreatorStuff();

	}
}
