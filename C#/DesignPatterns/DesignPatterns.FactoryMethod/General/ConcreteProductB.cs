﻿using System;

namespace DesignPatterns.FactoryMethod.General
{
	/// <summary>
	/// Specific product implementation defined by IProduct
	/// </summary>
	public class ConcreteProductB : IProduct
	{
		/// <summary>
		/// This is common method implemented by all concrete products, which implements
		/// IProduct interface
		/// </summary>
		public void DoStuff()
		{
			var className = GetConcreteProductAName();
			Console.WriteLine($"{className}::{nameof(DoStuff)}");
		}

		/// <summary>
		/// This is very specific method for ConcreteProductB.
		/// </summary>
		/// <returns>Name of this class</returns>
		public string GetConcreteProductAName() => nameof(ConcreteProductB);
	}
}
