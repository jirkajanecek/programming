﻿namespace DesignPatterns.FactoryMethod.General
{
	/// <summary>
	/// Creator for concrete product A
	/// </summary>
	public class ConcreteCreatorA : Creator
	{
		/// <summary>
		/// Create concrete product A
		/// </summary>
		/// <returns>Return conrete product A</returns>
		public override IProduct CreateProduct()
		{
			var product = new ConcreteProductA();
			return product;
		}
	}
}
