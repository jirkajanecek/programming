﻿namespace DesignPatterns.FactoryMethod.General
{
	public enum ProductType
    {
		ProductA,
		ProductB
    }
}
