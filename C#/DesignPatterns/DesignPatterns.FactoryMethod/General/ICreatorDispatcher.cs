﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.FactoryMethod.General
{
	/// <summary>
	/// Creator dispatcher.
	/// </summary>
	public interface ICreatorDispatcher
    {
		ICreator Dispatch(ProductType productType);
    }
}
