﻿namespace DesignPatterns.FactoryMethod.General
{
	/// <summary>
	/// Creator for concrete product B
	/// </summary>
	public class ConcreteCreatorB : Creator
	{
		/// <summary>
		/// Create concrete product B
		/// </summary>
		/// <returns>Return conrete product A</returns>
		public override IProduct CreateProduct()
		{
			var product = new ConcreteProductB();
			return product;
		}
	}
}
