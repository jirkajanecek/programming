﻿using System;

namespace DesignPatterns.FactoryMethod.General
{
	/// <summary>
	/// Class responsible for dispatching corresponding creator for required type
	/// </summary>
	public class CreatorDispatcher : ICreatorDispatcher
	{
		/// <summary>
		/// Method, which dispatch corresponding creator
		/// </summary>
		/// <param name="productType">Product type, based on creator is dispatched</param>
		/// <returns>Corresponding concrete creator</returns>
		public ICreator Dispatch(ProductType productType)
		{
			ICreator creator = null;

			switch (productType)
			{
				case ProductType.ProductA:
					creator = new ConcreteCreatorA();
					break;
				case ProductType.ProductB:
					creator = new ConcreteCreatorB();
					break;
				default:
					throw new ArgumentException($"{productType.ToString()} is not supported");
			}

			return creator;
		}
	}
}
