﻿namespace DesignPatterns.FactoryMethod.General
{
	/// <summary>
	/// General interface describing what is needed by concrete products to implement
	/// </summary>
	public interface IProduct
    {
		/// <summary>
		/// Base method required by all products
		/// </summary>
		void DoStuff();
    }
}
