﻿using DesignPatterns.AbstractFactory.General;
using DesignPatterns.AbstractFactory.General.Products;
using System;
using System.Collections;
using System.Collections.Generic;

namespace DesignPatterns.AbstractFactory.Tests.General.ClassData
{
	public class AbstractFactoryRunnerTestsClassData : IEnumerable<object[]>
	{
		public IEnumerator<object[]> GetEnumerator()
		{
			//test set for Type 1 products
			yield return new object[]
			{
				ProductType.Type1,
				new Dictionary<string, Type>
				{
					["ProductA"] = typeof(ConcreteProductAType1),
					["ProductB"] = typeof(ConcreteProductBType1),
					["ProductC"] = typeof(ConcreteProductCType1)
				}
			};
			
			//test set for Type 2 products
			yield return new object[]
			{
				ProductType.Type2,
				new Dictionary<string, Type>
				{
					["ProductA"] = typeof(ConcreteProductAType2),
					["ProductB"] = typeof(ConcreteProductBType2),
					["ProductC"] = typeof(ConcreteProductCType2)
				}
			};
		}

		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
	}
}
