﻿using DesignPatterns.AbstractFactory.General;
using DesignPatterns.AbstractFactory.Tests.General.ClassData;
using System;
using System.Collections.Generic;
using Xunit;
using FluentAssertions;

namespace DesignPatterns.AbstractFactory.Tests.General
{
	public class AbstractFactoryRunnerTests
	{
		private readonly ICreatorDispatcher dispatcher;

		public AbstractFactoryRunnerTests()
		{
			dispatcher = new CreatorDispatcher();
		}

		[Theory]
		[ClassData(typeof(AbstractFactoryRunnerTestsClassData))]
		public void Create_AbstractFactory_Success(ProductType productType, IDictionary<string, Type> expectedProductTypes)
		{
			var creator = dispatcher.Dispatch(productType);

			var actualProductA = creator.CreateProductA();
			var expectedProductAType = expectedProductTypes["ProductA"];
			actualProductA.GetType().ToString().Should().BeEquivalentTo(expectedProductAType.ToString(), "Product type for Product A should be the same");

			var actualProductB = creator.CreateProductB();
			var expectedProductBType = expectedProductTypes["ProductB"];
			actualProductB.GetType().ToString().Should().BeEquivalentTo(expectedProductBType.ToString(), "Product type for Product B should be the same");

			var actualProductC = creator.CreateProductC();
			var expectedProductCType = expectedProductTypes["ProductC"];
			actualProductC.GetType().ToString().Should().BeEquivalentTo(expectedProductCType.ToString(), "Product type for Product C should be the same");
		}

		[Fact]
		public void Create_AbstractFactory_UnknownType_Throws()
		{
			Action action = () => dispatcher.Dispatch((ProductType)(-1));
			action.Should().Throw<ArgumentException>();
		}
	}
}
