using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace poc.configuration.basicBinding
{
    public static class ServiceCollectionExtension
    {
        public static void AddBasicBinding(
            this IServiceCollection services,
            IConfiguration configuration)
        {
            var settingsModel = new BasicBindingConfiguration();
            configuration.GetSection(BasicBindingConfiguration.SectionName).Bind(settingsModel);

            Console.WriteLine($"Foo setting: '{settingsModel.Foo}'");
            Console.WriteLine($"Buzz settings: '{settingsModel.Buzz}'");
        }
    }
}