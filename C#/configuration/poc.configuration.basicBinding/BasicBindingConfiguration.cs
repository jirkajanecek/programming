namespace poc.configuration.basicBinding
{
    public class BasicBindingConfiguration
    {
        public const string SectionName = "BasicBinding";

        public string Foo {set; get;}
        public string Buzz {set; get;}
    }
}