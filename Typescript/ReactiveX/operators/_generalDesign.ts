/**
1702
1703
1704
 */

import { Observable, of, PartialObserver, Subscriber, Subscription } from "rxjs";

// operator face
function operator(operatorParam: number) {

  // function consuming previous observable and returning transforming observable
  return (source: Observable<number>): Observable<number> => {
    
    // result observable, which interconnects source data to result ones
    const transformingObservable = new Observable<number>((subscriber: Subscriber<number>) => {

      // 1. by subscription to source observable chain is triggered
      // 2. interconnection and transformation of emitted value to new one
      const subscription: Subscription = source.subscribe({

        // 1. real observable core - transormation of emitted value
        // 2. subscriber is notified by transformed value
        next: (emittedValue: number) => subscriber.next(transformationFunction(emittedValue, operatorParam)),
        
        // 1. error comming from source observable is passed to subscriber
        // 2. this passage holds the chain uninterrupted
        error: (error: Error) => subscriber.error(error),
        
        // 1. signal of completness is passed to subscriber
        // 2. this passage holds the chain uninterrupted
        complete: () => subscriber.complete()

      } as PartialObserver<number>);

      // 1. clean up callback releasing subscripption
      // 2. this callback is returned when source observable is done (by error or by complete)
      return () => {
        subscription.unsubscribe();
      }

    });
    return transformingObservable;

  }

}

// transformation - observable core functionality
function transformationFunction(emittedValue: number, operatorParam: number): number {
  return emittedValue + operatorParam;
}

// operator usage
of(1,2,3)
  .pipe(
    operator(1701)
  )
  .subscribe(console.log);
