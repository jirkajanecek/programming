/**
Next: bcd
Next: bc
Next: b
Next: c
Complete...
 */

import { Observable, of, PartialObserver, Subscriber } from "rxjs";

function filterLetter(letter: string){
  return (source: Observable<string>) => {
    return new Observable<string>((subscriber: Subscriber<string>) => {
      source.subscribe({
        next: (value) => subscriber.next(value.replace(letter, '')),
        error: (error) => subscriber.error(error),
        complete: () => subscriber.complete()
      } as PartialObserver<string>);
    });
  }
}

of('abcd', 'abc', 'ab', 'c')
  .pipe(
    filterLetter('a'))
  .subscribe({
    next: (value) => console.log('Next: ' + value),
    error: (error) => console.error('Error: ' + error),
    complete: () => console.log('Complete...')
  } as PartialObserver<string>);


