/**
 *
Starting...
Observable: Emitting the first...
Observer #1: 1
Observable: Emitting the second...
Observer #1: 2
Observable: Completing...
Observer #1 finishing...
Observable: Emitting the first...
Observer #2: 1
Observable: Emitting the second...
Observer #2: 2
Observable: Completing...
Observer #2 finishing...
Unsubscribing...
Finishing...

 */

import { Observable, PartialObserver, Subscriber, Subscription } from "rxjs";

console.log('Starting...');

const coldObservable$ = new Observable<number>((subscriber: Subscriber<number>) => {

  console.log('Observable: Emitting the first...');
  subscriber.next(1);
  
  console.log('Observable: Emitting the second...');
  subscriber.next(2);
  
  console.log('Observable: Completing...');
  subscriber.complete();
});

const subscrition1: Subscription = coldObservable$.subscribe({

  next: (value) => console.log('Observer #1: ' + value),
  complete: () => console.log('Observer #1 finishing...')
} as PartialObserver<number>);

const subscrition2: Subscription = coldObservable$.subscribe({

  next: (value) => console.log('Observer #2: ' + value),
  complete: () => console.log('Observer #2 finishing...')
} as PartialObserver<number>);

console.log('Unsubscribing...');

subscrition1.unsubscribe();
subscrition2.unsubscribe();

console.log('Finishing...');
