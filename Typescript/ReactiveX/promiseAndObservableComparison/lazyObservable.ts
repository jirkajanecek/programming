/**
 * 
Before subscribe() is called
Inside Observable
Next processing
Completed
 * 
 */

import { Observable, PartialObserver } from "rxjs";

const observable$ = new Observable<void>(subscriber => {
  console.log('Inside Observable')
  subscriber.next();
  subscriber.complete();
})

console.log('Before subscribe() is called');

observable$.subscribe(
  {
    next: () => console.log('Next processing'),
    complete: () => console.log('Completed')
  } as PartialObserver<void>
);

