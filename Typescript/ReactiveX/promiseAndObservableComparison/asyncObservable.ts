/**
 * 
Before .subscribe()
Inside observable...
After calling .subscribe()
Inside setTimeout observable...
Next handler...
Finished...
 * 
 */

import { Observable, PartialObserver } from "rxjs";

const observable$ = new Observable<void>(subscriber => {
  
  console.log('Inside observable...')
  
  setTimeout(() => {
    
    console.log('Inside setTimeout observable...');
    
    subscriber.next();
    subscriber.complete();
  }, 0);
});

console.log('Before .subscribe()');

observable$.subscribe(
  {
    next: () => console.log('Next handler...'),
    complete: () => console.log('Finished...')
  } as PartialObserver<void>);

console.log('After calling .subscribe()');