/**
 * 
Before calling .subscribe()
Inside observable...
Next handler...
Finished
After calling .subscribe()
 *
 */

import { observable, Observable, PartialObserver } from "rxjs";

const observable$ = new Observable<void>(subscriber => {

  console.log('Inside observable...');

  subscriber.next();
  subscriber.complete();
})

console.log('Before calling .subscribe()');

observable$.subscribe({
  next: () => console.log('Next handler...'),
  complete: () => console.log('Finished')
} as PartialObserver<void>);

console.log('After calling .subscribe()');