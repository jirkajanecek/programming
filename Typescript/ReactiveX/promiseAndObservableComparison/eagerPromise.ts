/**
 * 
Inside Promise
Before processing calling then()
Processing promise
 * 
 */

const promise = new Promise<void>((resolve) => {
  console.log('Inside Promise');
  resolve();
});

console.log('Before processing calling then()');

promise.then(()=>{
  console.log('Processing promise');
})