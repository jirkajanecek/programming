/**
 * 
Inside promise...
Before calling then()
After calling .then()
Calling .then()
 *
 */

const asyncPromise = new Promise<void>((resolve) => {
  console.log('Inside promise...');
  resolve();
});

console.log('Before calling then()');

asyncPromise.then(() => {
  console.log('Calling .then()');
})

console.log('After calling .then()');