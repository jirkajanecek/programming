import { Observable, of, PartialObserver } from "rxjs";
import { catchError, map } from "rxjs/operators";

/**
 * 
 * 
1st operator pass: 1
2nd operator pass: 1
3rd operator pass: 1
4th operator pass: 1
6th operator pass: 1
Next: 1
1st operator pass: 2
2nd operator pass: 2
3rd operator pass: 2
4th operator pass: 2
6th operator pass: 2
Next: 2
1st operator pass: 3
2nd operator pass: 3
5th operator catchError: Error: Fail
6th operator pass: NaN
Next: NaN
Finished...
 * 
 */

const dataSource = of(1, 2, 3, 4, 5);
const subscription = dataSource
  .pipe(
    map((value: number) => { console.log('1st operator pass: ' + value); return value; }),
    map((value: number) => { console.log('2nd operator pass: ' + value); return value; }),
    map((value: number) => {

      if (value > 2) {
        throw new Error('Fail');
      }

      console.log('3rd operator pass: ' + value);
      return value;

    }),
    map((value: number) => { console.log('4th operator pass: ' + value); return value; }),
    catchError(value => {

      console.log('5th operator catchError: ' + value);

      // replacing original observable
      const replacingObservable: Observable<number> = of(NaN);
      return replacingObservable;

    }),
    map((value: number) => { console.log('6th operator pass: ' + value); return value; })
  )
  .subscribe({
    next: value => console.log('Next: ' + value),
    error: error => console.error('Error: ' + error),
    complete: () => console.log('Finished...')
  } as PartialObserver<number>);

subscription.unsubscribe();