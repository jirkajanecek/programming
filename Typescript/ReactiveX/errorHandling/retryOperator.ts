import { interval, PartialObserver } from "rxjs";
import { map, retry } from "rxjs/operators";

/**
 * Actual output:
Next: 0
Next: 1
Next: 2
Next: 0
Next: 1
Next: 2
Next: 0
Next: 1
Next: 2
Error: Error: Failing
 */

const dataSource$ = interval(5);
dataSource$
  .pipe(
    map(value => {

      if (value > 2) {
        throw new Error('Failing');
      }

      return value;
    }),
    retry(2)
  )
  .subscribe({
    next: value => console.log('Next: ' + value),
    error: error => console.error('Error: ' + error),
    complete: () => console.log('Finished...')
  } as PartialObserver<number>);
