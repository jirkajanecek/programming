import { Observable, of, PartialObserver, Subscription } from "rxjs";
import { map } from "rxjs/operators";

/**
 * Expected output:
 * 
 * Failed: Error: Error message
 * 
 */

const dataSource: Observable<number> = of(1, 2, 3);
const subscription: Subscription = dataSource
  .pipe(map((value: number) => { throw new Error('Error message')}))
  .subscribe({
    next: value => console.log('Next ' + value),
    error: error => console.error('Failed: ' + error),
    complete: () => console.log('Finished')
  } as PartialObserver<number>);

subscription.unsubscribe();