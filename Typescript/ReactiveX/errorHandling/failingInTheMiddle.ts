import { Observable, of, PartialObserver, Subscription } from "rxjs";
import { map } from "rxjs/operators";

/**
 * Expected output:
 * 
  1st operator pass: 1
  2nd operator pass: 1
  3rd operator pass: 1
  Next: 1
  1st operator pass: 2
  2nd operator pass: 2
  3rd operator pass: 2
  Next: 2
  1st operator pass: 3
  Error: Error: Failing
 * 
 * When pipeline fails no more emits are done.
 * When it fails in the middle no more operators are called in pipeline
 */

const dataSource: Observable<number> = of(1, 2, 3, 4);
const subscription: Subscription = dataSource
  .pipe(
    map((value: number) => { console.log('1st operator pass: ' + value); return value; }),
    map((value: number) => {

      if(value > 2) {
        throw new Error('Failing');
      }

      console.log('2nd operator pass: ' + value);
      return value;
    }),
    map((value: number) => { console.log('3rd operator pass: ' + value); return value; })
  )
  .subscribe({
    next: value => console.log('Next: ' + value),
    error: error => console.error('Error: ' + error),
    complete: () => console.log('Finished...')
  } as PartialObserver<number>);

subscription.unsubscribe();