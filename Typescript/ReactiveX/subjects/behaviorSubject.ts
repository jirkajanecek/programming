/**
Subscribing observer #1
Observer #1: 1701
Subject is emitting: 1
Observer #1: 1
Subject is emitting: 2
Observer #1: 2
Subscribing observer #2
Observer #2: 2
Subject is emitting: 3
Observer #1: 3
Observer #2: 3
Subscribing observer #3
Observer #3: 3
Observer #1: finishing...
Observer #2: finishing...
Observer #3: finishing...
 */

import { BehaviorSubject, PartialObserver } from "rxjs";

// 1701 is default value
const subject = new BehaviorSubject<number>(1701);

console.log('Subscribing observer #1');
subject.subscribe({  
  next: (value: number) => console.log("Observer #1: " + value),
  complete: () => console.log('Observer #1: finishing...')
} as PartialObserver<number>);

console.log('Subject is emitting: 1');
subject.next(1);

console.log('Subject is emitting: 2');
subject.next(2);

console.log('Subscribing observer #2');
subject.subscribe({  
  next: (value: number) => console.log("Observer #2: " + value),
  complete: () => console.log('Observer #2: finishing...')
} as PartialObserver<number>);

console.log('Subject is emitting: 3');
subject.next(3);

console.log('Subscribing observer #3');
subject.subscribe({  
  next: (value: number) => console.log("Observer #3: " + value),
  complete: () => console.log('Observer #3: finishing...')
} as PartialObserver<number>);

subject.complete();