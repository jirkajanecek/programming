/**
Subscribing observer #1
Creating observable...
Subscribing observer #2
Subscribing subject to observable...
Observer #1: 1
Observer #2: 1
Observer #1: 2
Observer #2: 2
Observer #1: 3
Observer #2: 3
Observer #1: finishing...
Observer #2: finishing...
Subscribing observer #3
Observer #3: finishing...
 */

import { Observable, PartialObserver, Subject } from "rxjs";

const subject = new Subject<number>();

console.log('Subscribing observer #1');
subject.subscribe({  
  next: (value: number) => console.log("Observer #1: " + value),
  complete: () => console.log('Observer #1: finishing...')
} as PartialObserver<number>);

console.log('Creating observable...');
const broadcastedObservable$: Observable<number> = new Observable<number>((subscriber) => {
  subscriber.next(1);
  subscriber.next(2);
  subscriber.next(3);
  subscriber.complete();
});

console.log('Subscribing observer #2');
subject.subscribe({  
  next: (value: number) => console.log("Observer #2: " + value),
  complete: () => console.log('Observer #2: finishing...')
} as PartialObserver<number>);

console.log('Subscribing subject to observable...');
broadcastedObservable$.subscribe(subject);

console.log('Subscribing observer #3');
subject.subscribe({  
  next: (value: number) => console.log("Observer #3: " + value),
  complete: () => console.log('Observer #3: finishing...')
} as PartialObserver<number>);