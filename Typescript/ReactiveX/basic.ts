import { of, Observable, Subscription, PartialObserver } from 'rxjs';
import { map } from 'rxjs/operators';

/**
 * Description
 * 
 * 1. creates observable from collection of number
 * 2. transforming emitted stream one by one by mapping function
 * 3. creates subscription and start the stream from observable
 *  a. when subscription is created myMapping function is called with
 *      every emitted value from observable
 * 4. disposing stuff by unsubscribing
 * 
 * Expected output:
 * 
 * Next 1702
 * Next 1703
 * Next 1704
 * Next 1705
 * Next 1706
 * Finished...
 **/

const dataSource: Observable<number> = of(1, 2, 3, 4, 5);

const subscription: Subscription = dataSource
    .pipe(map(myMapping))
    .subscribe({
        next: (value) => console.log('Next ' + value),
        error: () => console.error('Error...'),
        complete: () => console.log('Finished...')
    } as PartialObserver<number>);

subscription.unsubscribe();

function myMapping(value: number): number {
    return value + 1701;
}