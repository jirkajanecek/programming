import { PromiseProducer } from "../promiseProducer";

let producer = new PromiseProducer();

let promise = producer.successAsync(1701);
promise
    .then((value) => producer.successAsync(value))
    .then((value) => producer.successAsync(value))
    .then((value) => producer.successAsync(value))
    .then((lastValue) => console.log("Finishing with " + lastValue))
    .catch((reason) => console.error(reason));
