import { PromiseProducer } from "../promiseProducer";

let producer = new PromiseProducer();

let promise = producer.successAsync(1701);
promise
    .then(value => producer.failAsync(value))
    .catch(reason => {
        let typedReason = reason as Error;
        console.error(typedReason.message)})
    .then(() => console.log("Continuing after catch"));