import { PromiseProducer } from "../promiseProducer";

let producer = new PromiseProducer();

let promise = producer.successAsync(1701)
promise
    .then(value => producer.successAsync(value))
    .then(value => producer.failAsync(value))
    .then(value => producer.successAsync(value))
    .catch(reason => console.error(reason.message));