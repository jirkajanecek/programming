export class PromiseProducer {

    /**
     * Function returns promise, which succeeds after 5 seconds with a
     * number passed as parameter
     * @param paramNumber just a number
     */
    successAsync(paramNumber: number): Promise<number>{
        
        console.log("Resolving with value: " + paramNumber);

        let promise = new Promise<number>((resolve, reject) => {
            setTimeout(
                () => resolve(paramNumber+1),
                5000);
        });
        
        return promise;
    }

    /**
     * Function returns promise, which fails after 5 seconds delay
     * @param paramNumber just a number for function
     */
    failAsync(paramNumber: number): Promise<number>{
        
        console.log("Rejecting with value: " + paramNumber);

        let promise = new Promise<number>((resolve, reject) => {
            setTimeout(
                () => reject(new Error("Failing")),
                5000);
        });
        
        return promise;
    }
};