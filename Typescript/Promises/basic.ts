// fulfilled with void
Promise
    .resolve()
    .then(
        () => console.log("Fulfilled"), // called
        () => console.error("Rejected") // not called
    );

// rejected
Promise
    .reject()
    .then(
        () => console.log("Fulfilled"), // not called
        () => console.error("Rejected") // called
    );

// resolved with value
Promise
    .resolve(1701)
    .then((value) => console.log(value));

// resolved with another promise
Promise
    .resolve(Promise.resolve(1701))
    .then((value: number) => console.log(value));

// rejecting with reason
Promise
    .reject(new Error("Rejecting"))
    .catch((reason: Error) => console.error(reason.message));

// resolved promise constructed
let resolvedPromise = new Promise<number>((resolve, reject) => {
    resolve(1701);
});
resolvedPromise
    .then(
        value => console.log(value));

// rejected promise contructed
let rejectedPromise = new Promise<number>((resolve, reject) => {
    reject(new Error('Failing imediately'))
});
rejectedPromise
    .catch(
        (reason: Error) => console.error(reason.message));