/**
 * 
Before main...
Before await...
After main...
1701
After await: 1701
 * 
 */

async function funcAsync(increment: number): Promise<number> {
    return 1701 + increment;
}

// promise based processing
/*funcAsync()
    .then(result => console.log(result))
    .catch((reason: Error) => console.error(reason.message));*/

// async/await based processing
async function main1(): Promise<void> {
    try {
      
      console.log('Before await...');

      let result = await funcAsync(0);
      console.log('After await #1: ' + result);

      result = await funcAsync(1);
      console.log('After await #2: ' + result);

      result = await funcAsync(2);
      console.log('After await #3: ' + result);
    }
    catch(reason) {
      console.error((reason as Error).message);
    }

    console.log('Exiting main()...');
}

console.log('Before main...');

main1().catch();

console.log('After main...');