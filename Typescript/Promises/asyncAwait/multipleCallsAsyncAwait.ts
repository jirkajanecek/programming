/**
Before main()
Before calling v()
inside v
After main()
1
Before calling w()
inside w
2
Before calling x()
inside x
3
Before calling y()
inside y
4
Before calling z()
inside z
5
*/

async function v(): Promise<number> {
  console.log('inside v');
  return 1;
}

async function w(): Promise<number> {
  console.log('inside w');
  return 2;
}

async function x(): Promise<number> {
  console.log('inside x');
  return 3;
}

async function y(): Promise<number> {
  console.log('inside y');
  return 4;
}

async function z(): Promise<number> {
  console.log('inside z');
  return 5;
}

async function main() : Promise<void>{
  
  console.log('Before calling v()');
  const one = await v();
  console.log(one);

  console.log('Before calling w()');
  const two = await w();
  console.log(two);

  console.log('Before calling x()');
  const three = await x();
  console.log(three);

  console.log('Before calling y()');
  const four = await y();
  console.log(four);

  console.log('Before calling z()');
  const five = await z();
  console.log(five);

  return;
}

console.log('Before main()');

main().catch(console.error);

console.log('After main()');