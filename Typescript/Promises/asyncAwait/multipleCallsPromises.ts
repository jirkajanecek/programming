/**
Before main()
Before calling v()
inside v
After main()
1
Before calling w()
inside w
2
Before calling x()
inside x
3
Before calling y()
inside y
4
Before calling z()
inside z
5
*/

const vP = () => new Promise<number>((resolve) => {
    console.log('inside v');
    resolve(1);
  });

const wP = () => new Promise<number>((resolve) => {
    console.log('inside w');
    resolve(2);
  });

const xP = () => new Promise<number>((resolve) => {
    console.log('inside x');
    resolve(3);
  });

const yP = () => new Promise<number>((resolve) => {
    console.log('inside y');
    resolve(4);
  });

const zP = () => new Promise<number>((resolve) => {
    console.log('inside z');
    resolve(5);
  });

function mainP(): Promise<void> {
  
  console.log('Before calling v()');
  
  return vP().then(console.log)
    .then(() => console.log('Before calling w()')).then(wP).then(console.log)
    .then(() => console.log('Before calling x()')).then(xP).then(console.log)
    .then(() => console.log('Before calling y()')).then(yP).then(console.log)
    .then(() => console.log('Before calling z()')).then(zP).then(console.log);
}

console.log('Before main()');

mainP().catch(console.error);

console.log('After main()');