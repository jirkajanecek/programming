import { printMessage } from "../src/HelloWorld";

describe("Basic Hello wolrd test", () => {
    
    it("Always returs true", () => {
        let result = printMessage("Test message");
        expect(result).toBe(true);
    });
});
