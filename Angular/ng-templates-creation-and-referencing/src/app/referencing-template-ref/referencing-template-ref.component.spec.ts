import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferencingTemplateRefComponent } from './referencing-template-ref.component';

describe('ReferencingTemplateRefComponent', () => {
  let component: ReferencingTemplateRefComponent;
  let fixture: ComponentFixture<ReferencingTemplateRefComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReferencingTemplateRefComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferencingTemplateRefComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
