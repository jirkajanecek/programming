import { AfterViewInit, Component, TemplateRef, ViewChild } from '@angular/core';

@Component({
  selector: 'referencing-template-ref',
  templateUrl: './referencing-template-ref.component.html',
  styleUrls: ['./referencing-template-ref.component.less']
})
export class ReferencingTemplateRefComponent implements AfterViewInit {
  
  @ViewChild(TemplateRef)
  public injectedReference!: TemplateRef<any>;

  ngAfterViewInit(): void {
    
    console.log(this.injectedReference);
  }
}
