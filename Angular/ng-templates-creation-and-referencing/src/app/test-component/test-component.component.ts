import { Component, Input } from '@angular/core';

@Component({
  selector: 'test-component',
  templateUrl: './test-component.component.html',
  styleUrls: ['./test-component.component.less']
})
export class TestComponent {

  @Input()
  public color = 'white';

}
