import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ng-template-outlet',
  templateUrl: './ng-template-outlet.component.html',
  styleUrls: ['./ng-template-outlet.component.less']
})
export class NgTemplateOutletComponent implements OnInit {

  public internalContext: any;

  constructor() {
    this.internalContext = {
      stringProperty: 'kua',
      numberProperty: 8
    };
  }

  ngOnInit(): void {
    console.log(this.internalContext);
  }

}
