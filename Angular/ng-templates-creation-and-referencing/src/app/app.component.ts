import { AfterViewInit, Component, OnInit, TemplateRef, ViewChild } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements AfterViewInit, OnInit{

  @ViewChild('green', { static: true })
  public greenTemplateRef!: TemplateRef<any>;
  
  }

  ngOnInit(): void {
    console.log('Init: ' + this.greenTemplateRef);
  }

  ngAfterViewInit(): void {
    console.log('AfterView: ' + this.greenTemplateRef);
  }
}
