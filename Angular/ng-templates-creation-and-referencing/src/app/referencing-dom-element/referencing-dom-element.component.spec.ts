import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferencingDomElementComponent } from './referencing-dom-element.component';

describe('ReferencingDomElementComponent', () => {
  let component: ReferencingDomElementComponent;
  let fixture: ComponentFixture<ReferencingDomElementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReferencingDomElementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferencingDomElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
