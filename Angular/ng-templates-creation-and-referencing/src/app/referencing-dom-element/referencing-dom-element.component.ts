import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';

@Component({
  selector: 'referencing-dom-element',
  templateUrl: './referencing-dom-element.component.html',
  styleUrls: ['./referencing-dom-element.component.less']
})
export class ReferencingDomElementComponent implements AfterViewInit{
  
  @ViewChild('yellowBlock')
  private testedDomElement!: ElementRef;

  ngAfterViewInit(): void {
    
    console.log(this.testedDomElement);

    console.log(this.testedDomElement.nativeElement);
  }
}
