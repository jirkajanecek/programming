import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferencingComponentComponent } from './referencing-component.component';

describe('ReferencingComponentComponent', () => {
  let component: ReferencingComponentComponent;
  let fixture: ComponentFixture<ReferencingComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReferencingComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferencingComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
