import { AfterViewInit } from '@angular/core';
import { Component, ViewChild } from '@angular/core';
import { TestComponent } from '../test-component/test-component.component';

@Component({
  selector: 'referencing-component',
  templateUrl: './referencing-component.component.html',
  styleUrls: ['./referencing-component.component.less']
})
export class ReferencingComponentComponent implements AfterViewInit {

  @ViewChild(TestComponent)
  private referencedComponent!: TestComponent;
  
  ngAfterViewInit(): void {
    
    console.log(this.referencedComponent);
    
    console.log(
      'Resolved color from refernced component' +
      this.referencedComponent.color);
  }

}
