import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { NgTemplateOutletComponent } from './ng-template-outlet/ng-template-outlet.component';
import { ReferencingComponentComponent } from './referencing-component/referencing-component.component';
import { TestComponent } from './test-component/test-component.component';
import { ReferencingDomElementComponent } from './referencing-dom-element/referencing-dom-element.component';
import { ReferencingDomElementInsideComponentComponent } from './referencing-dom-element-inside-component/referencing-dom-element-inside-component.component';
import { ReferencingTemplateRefComponent } from './referencing-template-ref/referencing-template-ref.component';

@NgModule({
  declarations: [
    AppComponent,
    NgTemplateOutletComponent,
    ReferencingComponentComponent,
    TestComponent,
    ReferencingDomElementComponent,
    ReferencingDomElementInsideComponentComponent,
    ReferencingTemplateRefComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
