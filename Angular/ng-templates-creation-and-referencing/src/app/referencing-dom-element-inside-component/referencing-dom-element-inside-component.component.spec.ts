import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferencingDomElementInsideComponentComponent } from './referencing-dom-element-inside-component.component';

describe('ReferencingDomElementInsideComponentComponent', () => {
  let component: ReferencingDomElementInsideComponentComponent;
  let fixture: ComponentFixture<ReferencingDomElementInsideComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReferencingDomElementInsideComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferencingDomElementInsideComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
