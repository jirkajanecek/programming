import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';

@Component({
  selector: 'referencing-dom-element-inside-component',
  templateUrl: './referencing-dom-element-inside-component.component.html',
  styleUrls: ['./referencing-dom-element-inside-component.component.less']
})
export class ReferencingDomElementInsideComponentComponent implements AfterViewInit {
  
  @ViewChild('testComponentId', {read: ElementRef})
  private testedDomElement!: ElementRef;

  ngAfterViewInit(): void {

    console.log(this.testedDomElement);

    console.log(this.testedDomElement.nativeElement);
  }
}
