import {
  EventEmitter
} from '@angular/core';

import { 
  AfterViewInit,
  Component,
  QueryList,
  TemplateRef,
  ViewChild,
  ViewChildren,
  Output
} from '@angular/core';

import {
  TemplateContainer
} from '../models/templateContainer';

@Component({
  selector: 'template-defining',
  templateUrl: './template-defining.component.html',
  styleUrls: ['./template-defining.component.less']
})
export class TemplateDefiningComponent implements
  AfterViewInit {
  
  @ViewChild('blueTemplate')
  private blueTemplate: TemplateRef<never>;
  
  @ViewChildren(TemplateRef)
  private definedTemplates: QueryList<TemplateRef<never>>;

  @Output()
  public templateReady = new EventEmitter<TemplateContainer>();
  
  ngAfterViewInit(): void {
    console.log('TemplateDefiningComponent::ngAfterViewInit');

    const templateContainer = new TemplateContainer();
    templateContainer.multipleTemplates = this.definedTemplates.toArray();
    templateContainer.singleTemplate = this.blueTemplate;
    console.log(templateContainer);

    console.log('Emitting event with template container');
    this.templateReady.emit(templateContainer);
  }
}
