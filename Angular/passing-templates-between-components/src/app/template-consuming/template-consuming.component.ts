import {
  Component
} from '@angular/core';

import {
  TemplateContainer
} from '../models/templateContainer';

@Component({
  selector: 'template-consuming',
  templateUrl: './template-consuming.component.html',
  styleUrls: ['./template-consuming.component.less']
})
export class TemplateConsumingComponent {
  
  public templateContainer: TemplateContainer;

  public processTemplates(container: TemplateContainer): void {
    console.log('Receiving template container...');
    
    setTimeout(
      () => { this.templateContainer = container; },
      0);
  }
}
