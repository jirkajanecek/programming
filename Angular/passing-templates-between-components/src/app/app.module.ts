import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { TemplateDefiningComponent } from './template-defining/template-defining.component';
import { TemplateConsumingComponent } from './template-consuming/template-consuming.component';

@NgModule({
  declarations: [
    AppComponent,
    TemplateDefiningComponent,
    TemplateConsumingComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
