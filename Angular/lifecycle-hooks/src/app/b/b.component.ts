import {
  Component, Input
} from '@angular/core';

import {
  HooksLogger
} from '../hooks-logger';

@Component({
  selector: 'app-b',
  templateUrl: './b.component.html',
  styleUrls: ['./b.component.less']
})
export class BComponent extends HooksLogger {

  @Input('input')
  bComponentInput: string;

  constructor() {
    super("B");
  }
}
