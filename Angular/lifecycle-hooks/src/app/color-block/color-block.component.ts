import { 
  Component,
  Input
} from '@angular/core';

@Component({
  selector: 'color-block',
  templateUrl: './color-block.component.html',
  styleUrls: ['./color-block.component.less']
})
export class ColorBlockComponent {

  @Input()
  backgroundColor: string;

  @Input()
  displayName: string;
}
