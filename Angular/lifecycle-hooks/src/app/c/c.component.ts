import {
  Component, Input
} from '@angular/core';

import {
  HooksLogger
} from '../hooks-logger';

@Component({
  selector: 'app-c',
  templateUrl: './c.component.html',
  styleUrls: ['./c.component.less']
})
export class CComponent extends HooksLogger {

  @Input('input')
  cComponentInput: string;
  
  constructor() {
    super("C");
  }
}
