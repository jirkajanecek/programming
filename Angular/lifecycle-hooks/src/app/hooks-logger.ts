import { 
  DoCheck,
  AfterContentInit,
  OnInit,
  OnChanges,
  AfterContentChecked,
  AfterViewChecked,
  AfterViewInit,
  OnDestroy,
  SimpleChanges,
  Directive
} from "@angular/core";

@Directive()
export class HooksLogger implements
  OnChanges,
  OnInit,
  DoCheck,
  AfterContentInit,
  AfterContentChecked,
  AfterViewInit,
  AfterViewChecked,
  OnDestroy {

  constructor(
    private displayName: string) {
    console.log(this.displayName + '::ctor()');
  }

  ngAfterViewInit(): void {
    console.log(this.displayName + '::ngAfterViewInit()');
  }

  ngAfterViewChecked(): void {
    console.log(this.displayName + '::ngAfterViewChecked()');
  }
  
  ngOnDestroy(): void {
    console.log(this.displayName + '::ngOnDestroy()');
  }

  ngAfterContentChecked(): void {
    console.log(this.displayName + '::ngAfterContentChecked()');
  }

  ngAfterContentInit(): void {
    console.log(this.displayName + '::ngAfterContentInit()');
  }
  
  ngDoCheck(): void {
    console.log(this.displayName + '::ngDoCheck()');
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(this.displayName + '::ngOnChanges()');
    console.log(changes);
  }

  ngOnInit(): void {
    console.log(this.displayName + '::ngOnInit()');
  }
}