import { 
  Component, Input
} from '@angular/core';

import {
  HooksLogger
} from '../hooks-logger';

@Component({
  selector: 'app-a',
  templateUrl: './a.component.html',
  styleUrls: ['./a.component.less']
})
export class AComponent extends HooksLogger {
  
  @Input('input')
  aComponentInput: string;
  
  constructor() {
    super("A")
  }
}
