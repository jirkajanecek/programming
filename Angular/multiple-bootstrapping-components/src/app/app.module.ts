import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BlueComponent } from './blue/blue.component';
import { OrangeComponent } from './orange/orange.component';
import { GreenComponent } from './green/green.component';
import { RedComponent } from './red/red.component';

@NgModule({
  declarations: [
    BlueComponent,
    OrangeComponent,
    GreenComponent,
    RedComponent],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [
    BlueComponent,
    OrangeComponent,
    RedComponent
  ]
})
export class AppModule { }
