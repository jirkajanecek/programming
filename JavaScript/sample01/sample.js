function sayHello(){
    console.log('Saying Hello...');
}

//main
console.log('Entering main...');

sayHello();

setTimeout(sayHello, 3000);

console.log('Exiting main...');