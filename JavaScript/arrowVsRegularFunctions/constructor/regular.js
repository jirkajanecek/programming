/**
 true
 */

function Car(color) {
  this.color = color;
}

// when new is used redCar is type of Car
const redCar = new Car('red');
console.log(redCar instanceof Car);