/**
...
const redCar = new Car('red');
               ^
TypeError: Car is not a constructor
 */

const Car = (color) => {
  this.color = color;
}

// lexically 'global' object is passed
const redCar = new Car('red');
console.log(redCar instanceof Car);