/**
(2) ['c', 'd']
 */

function myFunction() {

  const arrowFunction = (...args) => {
    console.log(args);
  }

  arrowFunction('c', 'd');
}

myFunction('a', 'b');