/**
Arguments(2) ['a', 'b', callee: ƒ, Symbol(Symbol.iterator): ƒ]
 */

function myFunction() {
  
  const arrowFunction = () => {
    console.log(arguments);
  }
  
  arrowFunction('c', 'd');
}

myFunction('a', 'b');