/**
 Arguments(2) ['a', 'b', callee: ƒ, Symbol(Symbol.iterator): ƒ]
 */

function myFunction() {
  console.log(arguments);
}

myFunction('a', 'b');