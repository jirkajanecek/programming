/**
 * {method: ƒ}
 */

const myObject = {
  method() {
    console.log(this);
  }
};

myObject.method();