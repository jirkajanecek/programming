class A {

  numberPropety = 10;

  regularMethod() {
    console.log(this.numberPropety)
    console.log(this);
  }

  arrowMethod = () => {
    console.log(this.numberPropety);
    console.log(this);
  }
}

new A().arrowMethod();
new A().regularMethod();