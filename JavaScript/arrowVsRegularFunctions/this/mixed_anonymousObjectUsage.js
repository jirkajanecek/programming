/**
undefined {}
10 {numberProperty: 10, arrowMethod: ƒ, regularFunction: ƒ}
 */

var obj = {

  numberProperty: 10,

  arrowMethod: () => {
    console.log(this.numberProperty);
    console.log(this);
  },

  regularFunction: function(){ 
    console.log(this.numberProperty);
    console.log(this);
  }
};

obj.arrowMethod();
obj.regularFunction();