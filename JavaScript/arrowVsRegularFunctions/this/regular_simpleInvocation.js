/**
 * global {global: global, clearInterval: ƒ, clearTimeout: ƒ, setInterval: ƒ, setTimeout: ƒ, …}
 */

function myFunction() {
  console.log(this);
}

myFunction();