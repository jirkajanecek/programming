/**
{signature: 'A'}
{signature: 'A'}
{signature: 'B'}
{signature: 'B'}
 */

function myFunction() {
  console.log(this);
}

const myObjectA = { signature: 'A' };
const myObjectB = { signature: 'B' };

myFunction.apply(myObjectA);
myFunction.call(myObjectA);

myFunction.apply(myObjectB);
myFunction.call(myObjectB);