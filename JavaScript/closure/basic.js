/**
Calling X()
innerVar=20 outerVar=10 globalVar=30
Calling X()
innerVar=20 outerVar=11 globalVar=31
Calling X()
innerVar=20 outerVar=12 globalVar=32
Calling Y()
innerVar=20 outerVar=10 globalVar=33
*/

let globalVar = 30;

function outer() {

  let outerVar = 10;

  function inner() {

    let innerVar = 20;

    console.log(
      'innerVar=' + innerVar + ' ' +
      'outerVar=' + outerVar + ' ' +
      'globalVar=' + globalVar);

    innerVar++;
    outerVar++;
    globalVar++;
  }

  return inner;
}

//usage

let X = outer();
let Y = outer();

console.log('Calling X()');
X();

console.log('Calling X()');
X();

console.log('Calling X()');
X();

console.log('Calling Y()');
Y();