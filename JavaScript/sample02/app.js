function sayHelloSync() {
    sayHelloCore('Saying hello sync...');
}

function sayHelloAsync() {
    sayHelloCore('Saying hello async...');
}

function sayHelloAsyncMicro(){
    sayHelloCore('Saying hello async micro...');
}

function sayHelloCore(greeting){
    console.log(greeting);
}

console.log('app starting...');

sayHelloSync();

setTimeout(sayHelloAsync, 2000);

requestAnimationFrame(sayHelloAsyncMicro);

console.log('app finishing...');
